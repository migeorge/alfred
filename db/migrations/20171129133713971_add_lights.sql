-- +micrate Up
CREATE TABLE lights (
  id BIGSERIAL PRIMARY KEY,
  name VARCHAR,
  chip_id VARCHAR,
  location_id BIGINT,
  created_at TIMESTAMP,
  updated_at TIMESTAMP
);

-- +micrate Down
DROP TABLE IF EXISTS lights;
