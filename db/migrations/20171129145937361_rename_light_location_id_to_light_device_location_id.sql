-- +micrate Up
ALTER TABLE lights RENAME COLUMN location_id TO device_location_id;

-- +micrate Down
ALTER TABLE lights RENAME COLUMN device_location_id TO location_id;
