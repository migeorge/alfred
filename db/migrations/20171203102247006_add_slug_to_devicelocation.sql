-- +micrate Up
ALTER TABLE devicelocations ADD COLUMN slug VARCHAR;

-- +micrate Down
ALTER TABLE devicelocations DROP COLUMN slug;
