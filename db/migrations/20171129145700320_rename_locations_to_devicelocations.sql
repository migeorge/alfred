-- +micrate Up
ALTER TABLE locations RENAME TO devicelocations;

-- +micrate Down
ALTER TABLE devicelocations RENAME TO locations;
