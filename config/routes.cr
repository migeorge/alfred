Amber::Server.configure do |app|
  pipeline :web do
    plug Amber::Pipe::Error.new
    plug Amber::Pipe::Logger.new
    plug Amber::Pipe::Session.new
    plug Amber::Pipe::Flash.new
    plug Amber::Pipe::CSRF.new

    plug Authenticate.new
  end

  # All static content will run these transformations
  pipeline :static do
    plug Amber::Pipe::Error.new
    plug HTTP::StaticFileHandler.new("./public")
    plug HTTP::CompressHandler.new
  end

  pipeline :sockets do
    plug Amber::Pipe::Error.new
    plug Amber::Pipe::Logger.new
  end

  pipeline :api do
    plug Amber::Pipe::Error.new
    plug Amber::Pipe::Logger.new
    plug Amber::Pipe::Session.new
  end

  routes :static do
    # Each route is defined as follow
    # verb resource : String, controller : Symbol, action : Symbol
    get "/*", Amber::Controller::Static, :index
  end

  routes :web do
    get "/signin", SessionController, :new
    post "/session", SessionController, :create
    get "/signout", SessionController, :delete

    get "/", HomeController, :index

    get "/lights", LightsController, :index
    get "/lights/:id", LightsController, :show
    get "/lights/:id/action/:action", LightsController, :action
    get "/lights/:id/edit", LightsController, :edit
    post "/lights/:id/update", LightsController, :update
    get "/lights/:id/destroy", LightsController, :destroy

    resources "/locations", LocationsController
    get "/locations/:id/destroy", LocationsController, :destroy
  end

  routes :sockets do
    websocket "/api/v1/sockets/light", LightSocket
    get "/api/v1/sockets/light", LightsController, :socket
  end

  routes :api do
    post "/api/v1/google_assistant", GoogleAssistantController, :create
  end
end
