require "./initializers/**"
require "amber"
require "../src/models/**"
require "../src/handlers/**"

# load the application_controller before controllers which depend on it
require "../src/controllers/application_controller"
require "../src/controllers/**"

require "../src/sockets/**"

# NOTE: Settings should all be in config/environments/env.yml.
# Anything here will overwrite all environments.
Amber::Server.configure do |setting|
  ENV["PORT"] ||= "3000"
  # Server options
  # setting.name = "Alfred web application."
  setting.port = ENV["PORT"].to_i # Port you wish your app to run
  # setting.log = ::Logger.new(STDOUT)
  # setting.log.level = ::Logger::INFO
end
