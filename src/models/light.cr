require "granite_orm/adapter/pg"

class Light < Granite::ORM::Base
  adapter pg

  belongs_to :device_location

  primary id : Int64
  field name : String | Nil
  field chip_id : String
  timestamps

  def toggle_on
    LightSocket.message_light(chip_id, "on")
  end

  def toggle_off
    LightSocket.message_light(chip_id, "off")
  end

  def self.toggle_by_location(location_slug, action)
    if location = DeviceLocation.find_by(:slug, location_slug)
      lights = Light.by_location(location.id)

      lights.each do |l|
        if action == "on"
          l.toggle_on
        elsif action == "off"
          l.toggle_off
        end
      end
    else
      puts "Location not found"
    end
  end

  def self.by_location(location_id)
    self.all("WHERE device_location_id = #{location_id}")
  end

  def self.all_awaiting_setup
    self.all("WHERE device_location_id IS NULL")
  end

  def self.all_setup
    self.all("WHERE device_location_id IS NOT NULL")
  end
end
