require "granite_orm/adapter/pg"

class DeviceLocation < Granite::ORM::Base
  adapter pg
  before_save :generate_slug

  primary id : Int64
  field name : String
  field slug : String
  timestamps

  has_many :lights

  validate :name, "is required", -> (location : DeviceLocation) do
    (location.name != nil) && (!location.name.not_nil!.empty?)
  end

  def self.all_ordered
    self.all("ORDER BY devicelocations.name ASC")
  end

  def generate_slug
    if !@name.nil?
      @slug = @name.not_nil!.gsub(" ", "").downcase
    end
  end
end
