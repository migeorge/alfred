class LightChannel < Amber::WebSockets::Channel
  def handle_joined(client_socket, message)
    puts "client joined light channel"
  end

  def handle_message(client_socket, message)
    if message["topic"] && message["subject"]
      case message["subject"]
      when "ping"
        chip_id = message["topic"].to_s.split(":")[1]

        if light = Light.find_by(:chip_id, chip_id)
          # TODO: add last_seen to lights, when a light pings set last_seen to now
        else
          light = Light.new
          light.chip_id = chip_id
          light.save
        end

        puts "ping: #{chip_id}"
      else
        puts "unknown light channel message: #{message}"
      end
    end
  end

  def handle_leave(client_socket)
    puts "client left light channel"
  end
end
