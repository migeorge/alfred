struct LightSocket < Amber::WebSockets::ClientSocket
  channel "light:*", LightChannel

  def on_connect
    true
  end

  def self.message_light(chip_id, message)
    self.broadcast("message", "light:#{chip_id}", "msg:new", {
      "message" => message
    })
  end
end
