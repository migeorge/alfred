class LightsController < ApplicationController
  def index
    lights_awaiting_setup = Light.all_awaiting_setup
    lights = Light.all_setup

    render("index.slang")
  end

  def show
    if light = Light.find(params["id"])
      render("show.slang")
    else
      redirect_to("/lights")
    end
  end

  def action
    if light = Light.find(params["id"])
      action = params["action"]

      if action == "on"
        flash["success"] = "Toggled light on"
        light.toggle_on
      elsif action == "off"
        flash["success"] = "Toggled light off"
        light.toggle_off
      end

      redirect_to("/lights/#{light.id}")
    else
      redirect_to("/lights")
    end
  end

  def edit
    if light = Light.find(params["id"])
      locations = DeviceLocation.all_ordered

      render("edit.slang")
    else
      redirect_to("/lights")
    end
  end

  def update
    if light = Light.find(params["id"])
      light.set_attributes(light_params.validate!)
      light.device_location_id = params["device_location_id"].to_i64

      if light.valid? && light.save
        flash["success"] = "Updated light"
        redirect_to("/lights")
      else
        flash["danger"] = "Could not update light"

        locations = DeviceLocation.all_ordered
        render("edit.slang")
      end
    else
      redirect_to("/lights")
    end
  end

  def destroy
    if light = Light.find(params["id"])
      if light.destroy
        flash["success"] = "Deleted light"
      else
        flash["danger"] = "Could not delete light"
      end
    else
      flash["danger"] = "Could not delete light"
    end
    redirect_to("/lights")
  end

  def socket
    "hello"
  end

  private def light_params
    params.validation do
      required(:name) { |f| !f.nil? }
      required(:device_location_id) { |f| !f.nil? }
    end
  end
end
