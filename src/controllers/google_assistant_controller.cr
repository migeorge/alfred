class GoogleAssistantController < ApplicationController
  def create
    if params["queryResult"]
      queryResult = JSON.parse(params["queryResult"])
      location_slug = queryResult["parameters"]["room-name"]
      action = queryResult["parameters"]["switch-action"]

      Light.toggle_by_location(location_slug, action)

      response.status_code = 200
    else
      response.status_code = 400
    end
  end
end
