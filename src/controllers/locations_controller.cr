class LocationsController < ApplicationController
  def index
    locations = DeviceLocation.all_ordered
    render("index.slang")
  end

  def new
    render("new.slang")
  end

  def show; end

  def create
    location = DeviceLocation.new(location_params.validate!)

    if location.valid? && location.save
      flash["success"] = "Created Location sucessfully!"
      redirect_to("/locations")
    else
      flash["danger"] = "Could not create Location!"
      render("new.slang")
    end
  end

  def edit
    if location = DeviceLocation.find(params["id"])
      render("edit.slang")
    else
      redirect_to("/locations")
    end
  end

  def update
    if location = DeviceLocation.find(params["id"])
      location.set_attributes(location_params.validate!)

      if location.valid? && location.save
        flash["success"] = "Updated location"
        redirect_to("/locations")
      else
        flash["danger"] = "Could not update location"
        render("edit.slang")
      end
    else
      redirect_to("/locations")
    end
  end

  def destroy
    if location = DeviceLocation.find(params["id"])
      if location.destroy
        flash["success"] = "Deleted location"
      else
        flash["danger"] = "Could not delete location"
      end
    else
      flash["danger"] = "Could not delete location"
    end

    redirect_to("/locations")
  end

  private def location_params
    params.validation do
      required(:name) { |f| !f.nil? }
    end
  end
end
