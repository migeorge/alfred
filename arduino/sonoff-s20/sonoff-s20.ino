#include <WebSocketsClient.h>

#include <ESP8266WiFi.h>

#include <DNSServer.h>
#include <ESP8266WebServer.h>
#include <WiFiManager.h>

int relayPin = 12;
int buttonPin = 0;

char host[] = "alfred.migeorge.me";
char socketPath[] = "/api/v1/sockets/light";

unsigned char pingType[] = "ping";

WebSocketsClient webSocket;

String socketTopic;

String joinMessage() {
  return "{\"event\": \"join\", \"topic\": \"" + socketTopic + "\"}";
}

String socketMessage(String subject) {
  return "{ \"event\": \"message\", \"topic\": \"" + socketTopic + "\", \"subject\": \"" + subject + "\" }";
}

String socketMessage(String subject, String payload) {
  return "{ \"event\": \"message\", \"topic\": \"" + socketTopic + "\", \"subject\": \"" + subject + "\", \"payload\": \"" + payload + "\" }";
}

void webSocketEvent(WStype_t type, uint8_t * payload, size_t length) {
  switch(type) {
    case WStype_DISCONNECTED:
      Serial.printf("Socket disconnected!\n");
      break;
    case WStype_CONNECTED: {
      Serial.printf("Socket Connected to url: %s\n", payload);

      webSocket.sendTXT("{\"event\": \"join\", \"topic\": \"" + socketTopic + "\"}");
      webSocket.sendTXT("{ \"event\": \"message\", \"topic\": \"" + socketTopic + "\", \"subject\": \"ping\" }");
    }
      break;
    case WStype_TEXT:
      Serial.printf("Socket get text: %s\n", payload);
      String payloadStr = (char*)payload;

      if (payloadStr == "{\"event\":\"message\",\"topic\":\"" + socketTopic + "\",\"subject\":\"msg:new\",\"payload\":{\"message\":\"on\"}}") {
        digitalWrite(relayPin, HIGH);
      }

      if (payloadStr == "{\"event\":\"message\",\"topic\":\"" + socketTopic + "\",\"subject\":\"msg:new\",\"payload\":{\"message\":\"off\"}}") {
        digitalWrite(relayPin, LOW);
      }

      break;
  }
}

void setup() {
  pinMode(relayPin, OUTPUT);
  pinMode(buttonPin, OUTPUT);

  digitalWrite(relayPin, HIGH);
  
  Serial.begin(115200);

  WiFiManager wifiManager;
  wifiManager.autoConnect();

  socketTopic = "light:" + String(ESP.getChipId());

  Serial.println("Connected to WiFi");

  webSocket.begin(host, 80, socketPath);
  webSocket.onEvent(webSocketEvent);
  webSocket.setReconnectInterval(5000);
}

void loop() {
  webSocket.loop();
}
